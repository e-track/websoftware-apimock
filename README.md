# Running the Application

1. Create the image: `docker build -t phoebus-api-mock .`
2. Run the image: `docker run -p 3000:3000 -d phoebus-api-mock`
