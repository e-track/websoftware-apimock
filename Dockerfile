FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Bundle app source
COPY . .

# Map needed port
EXPOSE 3000

# Start the server
CMD ["npm", "start"]
