var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var configurationLabelsRouter = require('./routes/configuration-labels').router;
var authenticationLoginRouter = require('./routes/authentication-login').router;
var cabinetLocatorRouter = require('./routes/cabinet-locator').router;
var keyRouter = require('./routes/key').router;
var keyActivityRouter = require('./routes/key-activity').router;
var keyLocatorRouter = require('./routes/key-locator').router;
var usersRouter = require('./routes/users').router;
var groupsRouter = require('./routes/groups').router;
var groupRouter = require('./routes/group').router;
var roleRouter = require('./routes/role').router;
var userAccountRouter = require('./routes/user-account').router;
var editUserAccountRouter = require('./routes/edit-account').router;
var userCustomFieldsRouter = require('./routes/custom-fields').router;
var permissionsRouter = require('./routes/permissions').router;
var analyticsSystemRouter = require('./routes/analytics-system').router;

var app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/configuration-labels', configurationLabelsRouter);
app.use('/authentication', authenticationLoginRouter);
app.use('/cabinet-locator', cabinetLocatorRouter);
app.use('/key-locator', keyLocatorRouter);
app.use('/key', keyRouter);
app.use('/key-activity', keyActivityRouter);
app.use('/users/page/1', usersRouter);
app.use('/userGroup/1', groupRouter);
app.use('/userGroups', groupsRouter);
app.use('/role', roleRouter);
app.use('/user-account/1', userAccountRouter);
app.use('/edit-user-account/1', editUserAccountRouter);
app.use('/custom-fields/get', userCustomFieldsRouter);
app.use('/permissions/get', permissionsRouter);
app.use('/analytics/systemUsage', analyticsSystemRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err,
  });
});

module.exports = app;
