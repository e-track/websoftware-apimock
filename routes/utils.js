
/**
 *
 * @param {Date} date
 * @return {string}
 */
function formatDateOutput(date) {
    return date.toISOString().split('T')[0];
}

/**
 *
 * @param {Date} currentDate
 * @param {number} offset
 * @return {Date}
 */
function addDays(currentDate, offset) {
    const currentDateCopy = new Date(currentDate.getTime());
    return new Date(currentDateCopy.setDate(currentDateCopy.getDate() + offset));
}

/**
 *
 * @param {Date} currentDate
 * @param {number} offset
 * @return {Date}
 */
function addMonth(currentDate, offset) {
    const currentDateCopy = new Date(currentDate.getTime());
    return new Date(currentDateCopy.setMonth(currentDateCopy.getMonth() + offset));
}

module.exports = {
    formatDateOutput,
    addDays,
    addMonth,
}
