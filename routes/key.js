var express = require('express');
var router = express.Router();

var labels = require('../data/key/key.json');

router.get('/', function (req, res, next) {
  res.json(labels);
});

module.exports = {
  router,
};
