var express = require('express');
var router = express.Router();

var labels = require('../data/management/group.json');

router.get('/', function (req, res, next) {
  res.json(labels);
});

module.exports = {
  router,
};
