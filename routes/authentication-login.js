var express = require('express');
var router = express.Router();

var authentication = require('../data/authentication/login.json');

router.get('/:username', function (req, res, next) {
  var username = req.params.username;
  var response = authentication[username];
  res.json(response);
});

module.exports = {
  router,
};
